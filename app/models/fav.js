import DS from 'ember-data';

export default DS.Model.extend({
  url: DS.attr('string'),
  title: DS.attr('string'),
  type: DS.attr('string'),
  timestamp: DS.attr('number'),
});
