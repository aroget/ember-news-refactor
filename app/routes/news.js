import Ember from 'ember';

export default Ember.Route.extend({

  // model: function(params){
  //   var query = params.news_type;
  //   var articles = [];
  //   var nytQuery;
  //   var nprQuery;
  //   var guardianQuery;
  //
  //   if (query === 'world'){
  //     nytQuery = 'foreign';
  //     nprQuery = '1004';
  //     guardianQuery = 'world';
  //   }
  //   else if (query === 'business'){
  //     nytQuery = 'business';
  //     nprQuery = '1006';
  //     guardianQuery = 'business';
  //   }
  //   else if (query === 'sports'){
  //     nytQuery = 'sports';
  //     nprQuery = '1055';
  //     guardianQuery = 'sport';
  //   }
  //   var promises = {
  //     nyt: Ember.$.getJSON('http://api.nytimes.com/svc/search/v2/articlesearch.json?fq=news_desk:("'+nytQuery+'")&sort=newest&api-key=669ab6edf84162e1c0a6201f005eb6c6:13:72313743'),
  //     guardian: Ember.$.getJSON('http://content.guardianapis.com/search?section='+guardianQuery+'&api-key=7fhn6vtmzfxnm3w78dkk9hwe'),
  //     npr: Ember.$.getJSON('http://api.npr.org/query?id=' + nprQuery + '&apiKey=MDE5NTQ2MzU4MDE0MzQ0NTAwNDhlOTE5Yw001&format=json&numResults=10')
  //   };
  //
  //   Ember.RSVP.hash(promises).then(function(results) {
  //     var nytFeed      = results.nyt.response.docs;
  //     var guardianFeed = results.guardian.response.results;
  //     var nprFeed      = results.npr.list.story;
  //
  //     Ember.$.each(nprFeed, function(){
  //       var date = this.pubDate.$text;
  //       var cleanDate = date.slice(5, 16);
  //       var noSpacesDate = cleanDate.replace(/ /g, '');
  //
  //       articles.pushObject({
  //         'url'      : this.link[0].$text,
  //         'news-type': query,
  //         'source'   : 'National Public Radio',
  //         // 'date'  : moment(noSpacesDate, "YYYYMMDD").fromNow(),
  //         'title'    : this.title.$text
  //       });
  //     });
  //
  //     Ember.$.each(nytFeed, function(){
  //       var date = this.pub_date;
  //       var cleanDate = date.slice(0,10);
  //       var noSpacesDate = cleanDate.replace(/-/g, '');
  //
  //       articles.pushObject({
  //         'url'      : this.web_url,
  //         'news-type': query,
  //         'source'   : 'The New York Times',
  //         'date'     : moment(noSpacesDate, "YYYYMMDD").fromNow(),
  //         'title'    : this.headline.main
  //       });
  //     });
  //
  //     Ember.$.each(guardianFeed, function(){
  //       var date = this.webPublicationDate;
  //       var cleanDate = date.slice(0,10);
  //       var noSpacesDate = cleanDate.replace(/-/g, '');
  //
  //       articles.pushObject({
  //         'url'      : this.webUrl,
  //         'news-type': query,
  //         'source'   : 'The Guardian',
  //         'date'     : moment(noSpacesDate, "YYYYMMDD").fromNow(),
  //         'title'    : this.webTitle
  //       });
  //     });
  //   });
  //
  //   return articles;
  // }
});
