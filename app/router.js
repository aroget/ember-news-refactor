import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  // this.route('news', function(){
  //   this.route('news', {path: '/:news_type'});
  // })
  this.route('news', {path: '/news/:news_type'});
  this.route('favs');
});

export default Router;
