var express = require('express');
var mongoose = require('mongoose');

var app = express();

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

mongoose.connect('mongodb://localhost/emberData');

var favsSchema = new mongoose.Schema({
	url: 'string',
	title: 'string',
	type: 'string',
  timestamp: 'number'
});

var FavsModel = mongoose.model('fav',favsSchema);

//New lines!
app.get('/api/',function(req,res) {
	res.send('Working');
});

app.get('/api/favs', function(req,res) {
	FavsModel.find({},function(err,docs) {
		if(err) {
			res.send({error:err});
		}
		else {
			res.send({favs:docs});
		}
	});
});
app.listen('4500');
