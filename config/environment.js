/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'ajax',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },
    contentSecurityPolicy: {
      'default-src': "https://*.firebaseio.com http://*.ionicframework.com http://fonts.googleapis.com http://resume.andresroget.com/favicon.png http://*/assets/vendor.css",
      'script-src': "'self' 'unsafe-inline' 'unsafe-eval' http://www.reddit.com http://*.socialmention.com http://*.guardianapis.com http://*.npr.org https://*.firebaseio.com use.typekit.net connect.facebook.net maps.googleapis.com maps.gstatic.com" ,
      'font-src': "'self' http://*.gstatic.com data: use.typekit.net http://*.ionicframework.com",
      'connect-src': "'self' http://localhost:4500 http://api.adnpr.org http://content.guardianapis.com http://*.npr.org http://*.reddit.com http://api.nytimes.com https://*.firebase.com wss://*.firebaseio.com https://roget-ember-news.firebaseio.com, ws://localhost:35729/livereload",
      'img-src': "'self' http://*.andresroget.com www.facebook.com p.typekit.net",
      'style-src': "'self' 'unsafe-inline' use.typekit.net http://*.googleapis.com http://*.ionicframework.com",
      'frame-src': "s-static.ak.facebook.com static.ak.facebook.com https://*.firebaseio.com www.facebook.com"
    },
    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
